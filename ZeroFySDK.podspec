Pod::Spec.new do |s|
  s.name             = 'ZeroFySDK'
  s.version          = '0.1.0'
  s.summary          = 'SDK para utilização do ZeroFy'
  s.swift_version    = '4'
  s.description      = 'SDK para utilização do ZeroFy'

  s.homepage         = 'http://www.zerofy.com'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ZeroFy' => 'contato@zerofy.com' }
  s.source           = { :git => 'https://bitbucket.org/inspira_tecnologia/zerofy-sdk-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = 'ZeroFySDK.framework/Headers/*.h', 'PIATunnel.framework/Headers/*.h', 'openssl.framework/Headers/*.h'
  s.source_files = 'ZeroFySDK.framework/Headers/*.h', 'PIATunnel.framework/Headers/*.h', 'openssl.framework/Headers/*.h'
  s.vendored_frameworks = 'ZeroFySDK.framework', 'PIATunnel.framework', 'openssl.framework'

  s.frameworks = 'NetworkExtension', 'SystemConfiguration', 'CoreTelephony'
  s.dependency 'SmiSdk-swift', '~> 3.6.1'

end
